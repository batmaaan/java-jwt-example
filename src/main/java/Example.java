import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.JWTVerifier;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Example {
    private static final Algorithm ALGORITHM_HS = Algorithm.HMAC256("secret");
    private static final JWTVerifier JWT_VERIFIER = JWT
            .require(ALGORITHM_HS)
            .acceptExpiresAt(Instant.now().plus(5, ChronoUnit.HOURS).toEpochMilli())
            .build();

    public static void main(String[] args) {
        User user = new User(123, "Jura");

        String token = encode(user);

        User decode = decodeToken(token);

        System.out.println("=========================");

        User fakeUser = decodeToken(token + "omg");

        if (fakeUser != null) {
            throw new RuntimeException("something wrong");
        }
    }

    private static String encode(User user) {
        String createdToken = JWT.create()
                .withClaim("id", user.getId())
                .withClaim("name", user.getName())
                .sign(ALGORITHM_HS);

        System.out.println("createUserJwToken: user=" + user + " result=" + createdToken);

        return createdToken;
    }

    private static User decodeToken(String jwtDecodeToken) {
        System.out.println("==========================================");

        try {
            var decodedJWT = JWT_VERIFIER.verify(jwtDecodeToken);


            int id = Integer.parseInt(String.valueOf(decodedJWT.getClaim("id")));
            String name = String.valueOf(decodedJWT.getClaim("name"));
            User user = new User(id, name);
            System.out.println("decodeJwtTokenToUser: user=" + user + " token=" + jwtDecodeToken);

            return user;
        } catch (Exception e) {
            System.out.println("OH MY GOOOOOOOOOOOOOOOOOOOOOOOOOOOD, FAKE TOKEN!!!!!");
            return null;
        }
    }


}
